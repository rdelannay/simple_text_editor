# import des librairies python
import curses
import sys
import argparse

# import des classes pour l'éditeur
from window import Window
from cursor import Cursor
from buffer import Buffer

# gestion du déplacement du curseur lorsque je frappe sur une touche du clavier
# qui n'est pas mappée, vers la droite pour ajouter une lettre et vers la gauche
# lors de la suppression d'une lettre

def right(window, buffer, cursor):
    cursor.right(buffer)
    window.down(buffer, cursor)
    window.horizontal_scroll(cursor)

def left(window, buffer, cursor):
    cursor.left(buffer)
    window.up(cursor)
    window.horizontal_scroll(cursor)

# def main(stdscr)

def main(stdscr):
    # ajout de l'argument filename à la commande pour executer l'éditeur
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()

    with open(args.filename) as file:
        buffer = Buffer(file.read().splitlines())
    
    # récupération des classes Window et Cursor pour paramétrer la fenêtre de
    # l'éditeur et déplacer le curseur dans le buffer
    window = Window(curses.LINES -1, curses.COLS -1)
    cursor = Cursor()
    
    while True:
        stdscr.erase()
        for row, line in enumerate(buffer[window.row:window.row + window.n_rows]):
            if row == cursor.row - window.row and window.col > 0:
                line = "«" + line[window.col + 1:]
            if len(line) > window.n_cols:
                line = line[:window.n_cols - 1] + "»"
            stdscr.addstr(row, 0, line)
        stdscr.move(*window.translate(cursor))
        
        k = stdscr.getkey()
        if ord(k) == 24:
            sys.exit(0)
        elif k == "KEY_UP":
            cursor.up(buffer)
            window.up(cursor)
            window.horizontal_scroll(cursor)
        elif k == "KEY_DOWN":
            cursor.down(buffer)
            window.down(buffer, cursor)
            window.horizontal_scroll(cursor)
        elif k == "KEY_LEFT":
            cursor.left(buffer)
            window.up(cursor)
            window.horizontal_scroll(cursor)
        elif k == "KEY_RIGHT":
            right(window, buffer, cursor)
            cursor.right(buffer)
            window.down(buffer, cursor)
            window.horizontal_scroll(cursor)
        elif k == "\n":
            buffer.split(cursor)
            right(window, buffer, cursor)
        elif k in ("KEY_DELETE", "\x04"):
            buffer.delete(cursor)
        elif k in ("KEY_BACKSPACE", "\x7f"):
            if (cursor.row, cursor.col) > (0, 0):
                left(window, buffer, cursor)
                buffer.delete(cursor)
        else:
            buffer.insert(cursor, k)
            for _ in k:
                right(window, buffer, cursor)


if __name__ == "__main__":
    curses.wrapper(main)

